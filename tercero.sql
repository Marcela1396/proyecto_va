-- Creación de la tabla va_saber_pro
-- Tabla compuesta a partir de todas las tablas ftp_sbpro desde el periodo 20121-2018
create table proyecto.va_saber_pro(
	estu_consecutivo  text not null primary key,
	estu_municipio text,
	punt_razonamiento_cuantitativo numeric not null,
	punt_lectura_critica numeric not null,
	punt_competencias_ciudadanas numeric not null,
	punt_ingles numeric not null,
	periodo text not null,
	nombre_institucion text,
	programa text,
	municipio_programa text,
	departamento_programa text,
	punt_razonamiento_cualitativo_escala numeric not null,
	punt_lectura_critica_escala numeric not null,
	punt_competencias_ciudadanas_escala numeric not null,
	punt_ingles_escala numeric not null,
);

//esto se hace para cada archivo ftp_sbpro_gen_ periodo 
insert into proyecto.va_saber_pro(estu_consecutivo,estu_municipio,punt_razonamiento_cuantitativo,punt_lectura_critica,punt_competencias_ciudadanas,punt_ingles,periodo,nombre_institucion,programa,municipio_programa,departamento_programa)
(
	select estu_consecutivo,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when mod_razona_cuantitat_punt is null then 0 else replace(mod_razona_cuantitat_punt,',','.')::numeric end,
	case when mod_lectura_critica_punt is null then 0 else replace(mod_lectura_critica_punt,',','.')::numeric end ,
	case when mod_competen_ciudada_punt is null then 0 else replace(mod_competen_ciudada_punt,',','.')::numeric end,
	case when mod_ingles_punt is null then 0  else replace(mod_ingles_punt,',','.')::numeric end,
	periodo,
	inst_nombre_institucion,
	estu_prgm_academico,
	estu_prgm_municipio,
	estu_prgm_departamento
	from archivos.ftp_sbpro_gen_2016
);

--identificar valor maximo del programa hasta 2015 por cada prueba para ingenieria de sistemas a nivel nacional 
select max(punt_lectura_critica) lec, max(punt_razonamiento_cuantitativo) raz, max(punt_competencias_ciudadanas) ciu, max(punt_ingles) ing  FROM proyecto.va_saber_pro pro 
WHERE 
pro.programa like 'INGENIERIA DE SISTEMAS%'::text and 
pro.periodo in(
'20131',
'20132',
'20133',
'20134',
'20142',
'20143',
'20152',
'20153',
'20154'
);

--identificar valor maximo de los saber 11 para estudiantes que cursaron sistemas hasta el 2014
select max(s11.punt_matematicas) mat, max(s11.punt_lectura_critica) lec, max(s11.punt_sociales) soc, max(s11.punt_ingles) ing FROM proyecto.va_saber_11 s11 join proyecto.va_cruces cr on s11.estu_consecutivo=cr.estu_consecutivo_11 join proyecto.va_saber_pro pro on cr.estu_consecutivo_pro=pro.estu_consecutivo
WHERE 
pro.programa like 'INGENIERIA DE SISTEMAS%'::text and 
s11.periodo in(
'20061',
'20062',
'20071',
'20072',
'20081',
'20082',
'20091',
'20092',
'20101',
'20102',
'20111',
'20112',
'20121',
'20122',
'20131',
'20132',
'20141'
);

// obtener el valor de los campos para cada uno 

update proyecto.va_saber_pro set punt_lectura_critica_escala=(100*punt_lectura_critica)/15.8,
punt_razonamiento_cuantitativo_escala=(100*punt_razonamiento_cuantitativo)/16.3,
punt_competencias_ciudadanas_escala=(100*punt_competencias_ciudadanas)/14.4,
punt_ingles_escala=(100*punt_ingles)/15
WHERE 
programa like 'INGENIERIA DE SISTEMAS%'::text and
proyecto.va_saber_pro.periodo in(
'20131',
'20132',
'20133',
'20134',
'20142',
'20143',
'20152',
'20153',
'20154'
); 

update proyecto.va_saber_pro set punt_lectura_critica_escala=(100*punt_lectura_critica)/300
punt_razonamiento_cuantitativo_escala=(100*punt_razonamiento_cuantitativo)/300,
punt_competencias_ciudadanas_escala=(100*punt_competencias_ciudadanas)/300,
punt_ingles_escala=(100*punt_ingles)/300
WHERE proyecto.va_saber_pro.periodo in(
'20162',
'20163',
'20172',
'20173',
'20182',
'20183',
'20184'
);


///para ver valor maximo con no acotados
select max(punt_lectura_critica) FROM proyecto.va_saber_11proyecto.va_saber_pro pro 
WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text and pro.periodo in(
--'20121',
--'20122',
--'20123',
--'20124',
'20131',
'20132',
'20133',
'20134',
'20142',
'20143',
'20152',
'20153',
'20154'
);

select max(punt_matematicas) FROM proyecto.va_saber_11 s11 join proyecto.va_cruces cr on s11.estu_consecutivo=cr.estu_consecutivo_11 join proyecto.va_saber_pro pro on cr.estu_consecutivo_pro=pro.estu_consecutivo
WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text and s11.periodo in(
'20061',
'20132',
'20133',
'20134',
'20142',
'20143',
'20152',
'20153',
'20154'
);



select max(s11.punt_ingles) FROM proyecto.va_saber_11 s11 join proyecto.va_cruces cr on s11.estu_consecutivo=cr.estu_consecutivo_11 join proyecto.va_saber_pro pro on cr.estu_consecutivo_pro=pro.estu_consecutivo
WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text and s11.periodo in(
'20061',
	  '20062',
'20071',
	  '20072',
'20081',
	  '20082',
'20091',
	  '20092',
'20101',
	  '20102',
'20111',
	  '20112',
'20121',
	  '20122',
	  '20131',
'20132',
'20141'
);

select max(punt_lectura_critica) FROM proyecto.va_saber_pro pro 
WHERE 
--pro.departamento_programa = 'NARIÑO'::text 
--AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND 
pro.programa = 'INGENIERIA DE SISTEMAS'::text and 
pro.periodo in(
--'20121',
--'20122',
--'20123',
--'20124',
'20131',
'20132',
'20133',
'20134',
'20142',
'20143',
'20152',
'20153',
'20154'
);

--