-- Creación de la tabla va_saber_11 
-- Tabla compuesta a partir de todas las tablas ftp_saber11 desde el periodo 20061-20152
create table proyecto.va_saber_11(
	estu_consecutivo  text not null primary key,
	estu_genero char(1) not null,
	estu_municipio text,
	punt_matematicas numeric not null,
	punt_lectura_critica numeric not null,
	punt_sociales numeric not null,
	punt_ingles numeric not null,
	periodo text not null
);

-- Inserción de datos

-- Parte 1 -----------------------------------------------------------------------------------------
-- Desde 20061 hasta 20141 las pruebas se denominan:
-- punt_matematicas
-- punt_lenguaje = lenguaje + filosofia 
-- punt_c_sociales
-- punt_ingles
insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20061
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20062
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20071
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20072
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20081
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20082
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20091
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20092
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20101
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20102
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20111
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20112
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20121
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20122
);
insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20131
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20132
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else replace(punt_matematicas,',','.')::numeric end,
	case when punt_lenguaje is null or punt_filosofia is null then 0 else ((replace(punt_lenguaje,',','.')::numeric)+(replace(punt_filosofia,',','.')::numeric))/2 end ,
	case when punt_c_sociales is null then 0 else replace(punt_c_sociales,',','.')::numeric end,
	case when punt_ingles is null then 0  else replace(punt_ingles,',','.')::numeric end,
	periodo
	from archivos.ftp_saber11_20141
);

-- PARTE 2 -----------------------------------------------------------------------------------------
-- A partir de 20142 en adelante las pruebas saber 11 cambia su estructura y el nombre de sus 
-- componentes se asemeja al de las pruebas Saber Pro
-- Nombre Pruebas:
-- punt_matematicas,
-- punt_lectura_critica
-- punt_sociales_ciudadanas
-- punt_ingles

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else punt_matematicas::numeric end,
	case when punt_lectura_critica is null then 0 else punt_lectura_critica::numeric end,
	case when punt_sociales_ciudadanas is null then 0 else punt_sociales_ciudadanas::numeric end,
	case when punt_ingles is null then 0  else punt_ingles::numeric end,
	periodo
	from archivos.ftp_saber11_20142
);


insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else punt_matematicas::numeric end,
	case when punt_lectura_critica is null then 0 else punt_lectura_critica::numeric end,
	case when punt_sociales_ciudadanas is null then 0 else punt_sociales_ciudadanas::numeric end,
	case when punt_ingles is null then 0  else punt_ingles::numeric end,
	periodo
	from archivos.ftp_saber11_20151
);

insert into proyecto.va_saber_11(estu_consecutivo,estu_genero,estu_municipio,punt_matematicas,punt_lectura_critica,punt_sociales,punt_ingles,periodo)
(
	select estu_consecutivo,
	case when estu_genero is null then 'X' else estu_genero end,
	case when estu_cod_reside_mcpio is null then '0' else estu_cod_reside_mcpio end,
	case when punt_matematicas is null then 0 else punt_matematicas::numeric end,
	case when punt_lectura_critica is null then 0 else punt_lectura_critica::numeric end,
	case when punt_sociales_ciudadanas is null then 0 else punt_sociales_ciudadanas::numeric end,
	case when punt_ingles is null then 0  else punt_ingles::numeric end,
	periodo
	from archivos.ftp_saber11_20152
);


-- Se considera las pruebas Saber 11 hasta el periodo 20152 porque 
-- para periodos posteriores no existe aun el registro de su prueba
-- Saber pro

--obtener valor scala para saber 11 para posteriores a 20142
alter table proyecto.va_saber_11 add column punt_matematicas_escala numeric; 
alter table proyecto.va_saber_11 add column punt_lectura_critica_escala numeric; 
alter table proyecto.va_saber_11 add column punt_sociales_escala numeric; 
alter table proyecto.va_saber_11 add column punt_ingles_escala numeric; 

update proyecto.va_saber_11 set punt_matematicas_escala=punt_matematicas, 
punt_lectura_critica_escala=punt_lectura_critica,
punt_sociales_escala=punt_sociales,
punt_ingles_escala=punt_ingles 
where periodo in (
'20142',
'20151',
'20152',
'20161',
'20162',
'20171',
'20172',
'20181'
);

--para anteriores a 20141
update proyecto.va_saber_11 set 
punt_matematicas_escala=(100*punt_matematicas)/127, 
punt_lectura_critica_escala=(100*punt_lectura_critica)/83.84, 
punt_sociales_escala=(100*punt_sociales)/92, 
punt_ingles_escala=(100*punt_ingles)/117.29 
where periodo in
('20061',
'20062',
'20071',
'20072',
'20081',
'20082',
'20091',
'20092',
'20101',
'20102',
'20112',
'20121',
'20122',
'20131',
'20132',
'20141') and estu_consecutivo in (
  select distinct 
    s11.estu_consecutivo
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE
  pro.programa like 'INGENIERIA DE SISTEMAS%'::text
)

