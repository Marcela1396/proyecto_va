#!/usr/bin/env python
# coding: utf-8

# In[11]:


periodo_filtro=""
#periodo_filtro="and substring(pro.periodo,0,5) not in('2013','2018')"
periodo_filtro="and substring(pro.periodo,0,5)  in('2014')"

#columnas razonamiento cuantitativo
columnas=['pro.punt_lectura_critica_escala','s11.punt_lectura_critica']


# In[12]:


sql=("SELECT pro.estu_consecutivo AS pro,"+
    "s11.estu_consecutivo AS c11,"+
    columnas[0]+" as pruebaEcaes,"+
    columnas[1]+" as prueba11,"+
    "pro.periodo AS periodo_ecaes,"+
    "s11.periodo AS periodo_11"+
   " FROM proyecto.va_saber_pro pro"+
     " JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro"+
     " JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11"+
  " WHERE pro.departamento_programa = 'NARIÑO'::text "+
  " AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text "
     +periodo_filtro+
 " ORDER BY pro.periodo;")


# In[13]:


import pandas as pd
import numpy as np
from sqlalchemy import create_engine, event
from urllib.parse import quote_plus


# In[14]:


# follows django database settings format, replace with your own settings
DATABASES = {
       'produccion':{
        'NAME': 'proyecto_va',
        'USER': 'postgres',
        'PASSWORD': 'Grias*20',
        'HOST': '10.10.10.117',
        'PORT': 5432,
    }

}

db_produccion=DATABASES['produccion']
prod_engine_string = "postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}".format(
    user = db_produccion['USER'],
    password = db_produccion['PASSWORD'],
    host = db_produccion['HOST'],
    port = db_produccion['PORT'],
    database = db_produccion['NAME'],
    fast_executemany=True
)

# create sqlalchemy engine
prod= create_engine(prod_engine_string)


# In[15]:


df=pd.read_sql_query(sql,prod)
prod.dispose()
df.head()


# In[16]:


#Formula zscore
def z_score(datos):
    z=(datos-datos.mean())/datos.std()
    return z


# In[18]:


#Calcular el rendimiento por inidividuo
df['pruebaecaes_rend']=df['pruebaecaes']/df['pruebaecaes'].max()
df['prueba11_rend']=df['prueba11']/df['prueba11'].max()


# In[19]:


#aplicar transformación zscore
df['pruebaecaes_norm']=z_score(df['pruebaecaes_rend'])
df['prueba11_norm']=z_score(df['prueba11_rend'])


# In[20]:


df.describe()


# In[21]:


#diferencia
df['va_norm']=df.pruebaecaes-df.prueba11
df['va']=df.pruebaecaes_rend-df.prueba11_rend


# In[22]:


df.describe()


# In[23]:


# library & dataset
#!pip install seaborn
import seaborn as sns

# Make boxplot for one group only
sns.boxplot( y=df["va"] )
#sns.plt.show()
df_aux=df.copy()


# # Filtrar Ouliers

# In[24]:


di=df_aux["va"].quantile(0.75)-df_aux["va"].quantile(0.25)

q3=df_aux["va"].quantile(0.75)
q3=q3+1.5*di

q1=df_aux["va"].quantile(0.25)
q1=q1-(1.5*di)

print(len(df_aux[df_aux.va>=q3]))
print(len(df_aux[df_aux.va<=q1]))

df_aux=df_aux[df_aux.va<q3]
df_aux=df_aux[df_aux.va>q1]

#len(df_aux[df_aux.va<df_aux["va"].quantile(0.25)*3*di])


# In[119]:


# library & dataset
#!pip install seaborn
import seaborn as sns


 
# Make boxplot for one group only
sns.boxplot(y=df_aux["va"] )
#sns.plt.show()


# In[25]:


import matplotlib.pyplot as plt
from scipy import stats
def normalidad_variable_numerica(col):
    stats.probplot(df_aux[col], plot=plt)
    plt.xlabel('Diagrama de Probabilidad(normal) de la variable {}'.format(col))
    plt.show()
normalidad_variable_numerica('va')


# # D’Agostino and Pearson’s

# In[26]:


#H0 es normal
#H1 no es normal

columnas_numericas = df_aux.select_dtypes(['int', 'float64']).columns
for num_col in columnas_numericas:
    _, pval = stats.normaltest(df_aux[num_col])
    #print(pval)
    if(pval < 0.05):
        print("Rechaza h0: Columna {} no sigue una distribución normal".format(num_col), pval)
    else:
        print("No Rechaza ho: Columna {}  sigue una distribución normal".format(num_col),pval)


# In[122]:


#Ho es normal
#H1 no es normal


#!pip install plotly
from scipy.stats import normaltest
#import plotly.plotly as py
#import plotly.graph_objs as go
#import plotly.figure_factory as ff

import numpy as np
import pandas as pd
import scipy

stat, p = normaltest(df_aux.va)

# interpret
alpha = 0.05
if p > alpha:
    msg = 'Sample looks Gaussian (fail to reject H0) No rechaza H0 Es normal'
else:
    msg = 'Sample does not look Gaussian (reject H0) No es Normal'

print(msg)
p


# In[123]:


#Ho es normal
#H1 no es normal
#import numpy as np
#from scipy.stats import kstest

#pvalue=kstest(df_aux.va,'norm').pvalue
#if pvalue<0.05:
#    print("Rechaza h0: Columna {} no sigue una distribución normal".format(num_col), pvalue)
#else:
#    print("No Rechaza ho: Columna {}  sigue una distribución normal".format(num_col),pvalue)


# # Shapiro-Wilk Test

# In[27]:


#Ho Es normal
#H1 No es normal

from scipy.stats import shapiro
stat, p = shapiro(df_aux.va)

# interpret
alpha = 0.05
if p < alpha:
    msg = 'rechaza H0: no es normal'
else:
    msg = 'No rechaza H0: es normal'
    
print(msg)
print("probabilidad:",p)
print("valor stat:",stat)


# # Anderson-Darling Test

# In[28]:


#H0: es normal
#H1: no es

from scipy.stats import anderson
import plotly.graph_objs as go

result = anderson(df_aux.va)
stat = round(result.statistic, 4)

p = 0
result_mat = []
for i in range(len(result.critical_values)):
    sl, cv = result.significance_level[i], result.critical_values[i]
    if result.statistic < result.critical_values[i]:
        msg = '(fail to reject H0) Es Normal'
    else:
        msg = '(reject H0) No es Normal'
    result_mat.append([len(df_aux.va), stat, sl, cv, msg])

trace = go.Table(
    header=dict(values=['<b>Sample Size</b>', '<b>Statistic</b>', '<b>Significance Level</b>', '<b>Critical Value</b>', '<b>Comment</b>'],
                line = dict(width=0),
                fill = dict(color='rgba(42,63,95,0.8)'),
                align = 'center',
                font = dict(
                    color = '#ffffff',
                    size = 12
                )),
    cells=dict(values=np.array(result_mat).T,
               line = dict(width=0),
               fill = dict(color=[['#EBF0F8', '#ffffff', '#EBF0F8', '#ffffff', '#EBF0F8']]),
               align = 'center',
               height = 40),
    columnwidth=[0.3, 0.25, 0.3, 0.25, 0.5])
layout = dict(
    height=300,
    margin=dict(
        l=5,
        r=5,
        t=30,
        b=0
    )
)
data = [trace]
andar_table = dict(data=data, layout=layout)

result_mat


# # Test Hipotesis

# In[29]:


#muestreo aleotorio 
import math 

def tobs(datos,tam_muestra):
    df_aux_t=datos.sample(n=tam_muestra)
 
    d=df_aux_t['va'].mean()
    n=df_aux_t['va'].count()
    sd=df_aux_t['va'].std()

    tObs=(d-0)/(sd/math.sqrt(n))
    return (tObs,n,d,sd)

resultado=tobs(df_aux,df_aux.va.count())
#resultado=tobs(df_aux,100)
resultado

#HO Md<=0 No aporta 
#H1 Md>0  aporta


# In[ ]:





# In[ ]:





# In[ ]:




