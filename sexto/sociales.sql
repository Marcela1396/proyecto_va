SELECT
    substring(pro.periodo,0,5) AS periodo_ecaes,
    substring(s11.periodo,0,5) AS periodo_s11,
    count(1),
    (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales_escala)/max(s11.punt_sociales_escala)) dif
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  and substring(pro.periodo,0,5)='2013'
  group BY  substring(pro.periodo,0,5), substring(s11.periodo,0,5) 
 order by (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales_escala)/max(s11.punt_sociales_escala)) desc;

