-- Obtención de Valor agregado en prueba de Ingles
-- en estudiantes de Ingenieria de Sistemas / Universidad de Nariño
-- Total 189 registros

-- Consulta que obtiene los registros de los estudiantes
-- con su puntaje saber 11 y saber pro correspondientes a la prueba
-- de ingles = ingles sin estandarizar
SELECT pro.estu_consecutivo AS pro,
    s11.estu_consecutivo AS c11,
    pro.punt_ingles as punt_ingles_pro,
    s11.punt_ingles as punt_ingles_11,
    pro.periodo AS periodo_ecaes,
    s11.periodo AS periodo_11
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  ORDER BY pro.periodo;



-- Prueba Saber Pro
-- Consulta que visualiza por periodo :
-- El conteo de estudiantes de Sistemas/Udenar que se presentaron a realizar la prueba
-- El puntaje Minimo, puntaje Maximo y promedio de la Prueba en dicho periodo
SELECT
    pro.periodo AS periodo_ecaes,
    count(1),
    min(pro.punt_ingles),
    max(pro.punt_ingles),
    avg(pro.punt_ingles)
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY pro.periodo;


-- Prueba Saber 11
-- Consulta que visualiza por periodo :
-- El conteo de estudiantes de Sistemas/Udenar que se presentaron a realizar la prueba
-- El puntaje Minimo, puntaje Maximo y promedio de la Prueba en dicho periodo
  SELECT
    s11.periodo AS periodo_s11,
    count(1),
    min(s11.punt_ingles),
    max(s11.punt_ingles),
    avg(s11.punt_ingles)
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY s11.periodo;

-- Agregar a la tabla va_saber_pro el puntaje ingles_escala

-- Si existe eliminarla
drop column punt_ingles_escala;

alter table proyecto.va_saber_pro add column punt_ingles_escala numeric;

-- Del periodo 2013 a 2015 : Escala por puntajes que van en una escala de 0 a 20
update proyecto.va_saber_pro set punt_ingles_escala=(100*punt_ingles)/20
WHERE departamento_programa = 'NARIÑO'::text and nombre_institucion = 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND programa = 'INGENIERIA DE SISTEMAS'::text
and proyecto.va_saber_pro.periodo in(
'20131',
'20133',
'20142',
'20143',
'20153'
); 
--> Total 150

-- Del periodo 20163-20183 : Escala por puntajes que van en una escala de 0 a 300
update proyecto.va_saber_pro set punt_ingles_escala=(100*punt_ingles)/300
WHERE departamento_programa = 'NARIÑO'::text and nombre_institucion = 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND programa = 'INGENIERIA DE SISTEMAS'::text
and proyecto.va_saber_pro.periodo in(
'20163',
'20173',
'20183'
);

--> Total 119

--> Total 269 Sin cruces previamente

-- Ahora bien haciendo el cruce de las pruebas saber 11 y pro
-- de estudiantes de Sistemas/Udenar tomando en cuanta ya los puntajes escalados

select pro.* from proyecto.va_saber_pro pro join proyecto.va_cruces c
ON pro.estu_consecutivo = c.estu_consecutivo_pro
where punt_ingles is not null;


-- Finalmente se obtiene valor agregado de ingles
-- (promedio(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(promedio(s11.punt_ingles)/max(s11.punt_ingles))

SELECT
    --s11.periodo AS periodo_s11,
    substring(pro.periodo,0,5) AS periodo_ecaes,
    count(1),
    min(pro.punt_ingles_escala)min_punt_ingles_escala,
    max(pro.punt_ingles_escala)max_punt_ingles_escala,
    avg(pro.punt_ingles_escala)prom_punt_ingles_escala,
    min(s11.punt_ingles)min_punt_ingles,
    max(s11.punt_ingles)max_punt_ingles,
    avg(s11.punt_ingles) prom_punt_ingles,
    (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles)/max(s11.punt_ingles)) dif
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY  substring(pro.periodo,0,5)
 order by (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles)/max(s11.punt_ingles)) desc;
  
  