-- Obtiene todas las diferencias de todas las pruebas 

SELECT
    substring(pro.periodo,0,5) AS periodo_ecaes,
    count(1),
    (avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas)/max(s11.punt_matematicas)) difcuant,
 
    (avg(pro.punt_lectura_critica_escala)/ max(pro.punt_lectura_critica_escala)) -(avg(s11.punt_lectura_critica)/max(s11.punt_lectura_critica)) diflect,
	
    (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales)/max(s11.punt_sociales)) difsocial,
	 
    (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles)/max(s11.punt_ingles)) dif_ingles
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY  substring(pro.periodo,0,5)
 order by periodo_ecaes asc;



SELECT
    --s11.periodo AS periodo_s11,
    substring(pro.periodo,0,5) AS periodo_ecaes,
    count(1),
    min(pro.punt_ingles_escala)min_punt_ingles_escala,
    max(pro.punt_ingles_escala)max_punt_ingles_escala,
    avg(pro.punt_ingles_escala)prom_punt_ingles_escala,
    min(s11.punt_ingles)min_punt_ingles,
    max(s11.punt_ingles)max_punt_ingles,
    avg(s11.punt_ingles) prom_punt_ingles,
    (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles)/max(s11.punt_ingles)) dif_ingles,
  	min(pro.punt_razonamiento_cuantitativo_escala)min_punt_razonamiento_cuantitativo_escala,
    max(pro.punt_razonamiento_cuantitativo_escala)max_punt_razonamiento_cuantitativo_escala,
    avg(pro.punt_razonamiento_cuantitativo_escala)prom_punt_razonamiento_cuantitativo_escala,
    min(s11.punt_matematicas)min_punt_matematicas,
    max(s11.punt_matematicas)max_punt_matematicas,
    avg(s11.punt_matematicas) prom_punt_matematicas,
    (avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas)/max(s11.punt_matematicas)) dif_razonamiento
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY  substring(pro.periodo,0,5)
 order by (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles)/max(s11.punt_ingles)) desc;
 


 -- Totales : Por periodo y prueba
 SELECT
    substring(pro.periodo,0,5) AS periodo_ecaes,
    count(1),
(avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas_escala)/max(s11.punt_matematicas_escala)) difcuant,
(avg(pro.punt_lectura_critica_escala)/ max(pro.punt_lectura_critica_escala)) -(avg(s11.punt_lectura_critica_escala)/max(s11.punt_lectura_critica_escala)) diflect,
(avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales_escala)/max(s11.punt_sociales_escala)) difsocial,
(avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles_escala)/max(s11.punt_ingles_escala)) dif_ingles
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY  substring(pro.periodo,0,5)
 order by periodo_ecaes 

-- Totales por prueba
SELECT
    count(1),
    (avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas_escala)/max(s11.punt_matematicas_escala)) difcuant,
    (avg(pro.punt_lectura_critica_escala)/ max(pro.punt_lectura_critica_escala)) -(avg(s11.punt_lectura_critica_escala)/max(s11.punt_lectura_critica_escala)) diflect,
    (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales_escala)/max(s11.punt_sociales_escala)) difsocial,
    (avg(pro.punt_ingles_escala)/ max(pro.punt_ingles_escala)) -(avg(s11.punt_ingles_escala)/max(s11.punt_ingles_escala)) dif_ingles
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text




-- Consulta para consultar valor agregado/por periodo de ecaes y de icfes
SELECT
    substring(pro.periodo,0,5) AS periodo_ecaes,
    s11.periodo AS periodo_s11,
    count(1),
    min(pro.punt_razonamiento_cuantitativo_escala)min_punt_razonamiento_cuantitativo_escala,
    max(pro.punt_razonamiento_cuantitativo_escala)max_punt_razonamiento_cuantitativo_escala,
    avg(pro.punt_razonamiento_cuantitativo_escala)prom_punt_razonamiento_cuantitativo_escala,
    min(s11.punt_matematicas)min_punt_matematicas,
    max(s11.punt_matematicas)max_punt_matematicas,
    avg(s11.punt_matematicas) prom_punt_matematicas,
    (avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas)/max(s11.punt_matematicas)) dif
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  and substring(pro.periodo,0,5)='2013'
  group BY  substring(pro.periodo,0,5),s11.periodo
 order by (avg(pro.punt_razonamiento_cuantitativo_escala)/ max(pro.punt_razonamiento_cuantitativo_escala)) -(avg(s11.punt_matematicas)/max(s11.punt_matematicas)) desc;