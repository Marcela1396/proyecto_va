-- Obtención de Valor agregado en prueba de Competencias ciudadanas
-- en estudiantes de Ingenieria de Sistemas / Universidad de Nariño
-- Total 189 registros

-- Consulta que obtiene los registros de los estudiantes
-- con su puntaje saber 11 y saber pro correspondientes a la prueba
-- de sociales =  ciudadanas sin estandarizar
SELECT pro.estu_consecutivo AS pro,
    s11.estu_consecutivo AS c11,
    pro.punt_competencias_ciudadanas,
    s11.punt_sociales,
    pro.periodo AS periodo_ecaes,
    s11.periodo AS periodo_11
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  ORDER BY pro.periodo;



-- Prueba Saber Pro
-- Consulta que visualiza por periodo :
-- El conteo de estudiantes de Sistemas/Udenar que se presentaron a realizar la prueba
-- El puntaje Minimo, puntaje Maximo y promedio de la Prueba en dicho periodo
SELECT
    pro.periodo AS periodo_ecaes,
    count(1),
    min(pro.punt_competencias_ciudadanas),
    max(pro.punt_competencias_ciudadanas),
    avg(pro.punt_competencias_ciudadanas)
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY pro.periodo;
  -------------------------


-- Prueba Saber 11
-- Consulta que visualiza por periodo :
-- El conteo de estudiantes de Sistemas/Udenar que se presentaron a realizar la prueba
-- El puntaje Minimo, puntaje Maximo y promedio de la Prueba en dicho periodo
  SELECT
    s11.periodo AS periodo_s11,
    count(1),
    min(s11.punt_sociales),
    max(s11.punt_sociales),
    avg(s11.punt_sociales)
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY s11.periodo;

-- Agregar a la tabla va_saber_pro el puntaje ciudadanas_escala

-- Si existe eliminarla
drop column punt_competencias_ciudadanas_escala;

alter table proyecto.va_saber_pro add column punt_competencias_ciudadanas_escala numeric;

-- Del periodo 2013 a 2015 : Escala por puntajes que van en una escala de 0 a 20
update proyecto.va_saber_pro set punt_competencias_ciudadanas_escala=(100*punt_competencias_ciudadanas)/20
WHERE departamento_programa = 'NARIÑO'::text and nombre_institucion = 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND programa = 'INGENIERIA DE SISTEMAS'::text
and proyecto.va_saber_pro.periodo in(
'20131',
'20133',
'20142',
'20143',
'20153'
);
--> Total 150

-- Del periodo 20163-20183 : Escala por puntajes que van en una escala de 0 a 300
update proyecto.va_saber_pro set punt_competencias_ciudadanas_escala=(100*punt_competencias_ciudadanas)/300
WHERE departamento_programa = 'NARIÑO'::text and nombre_institucion = 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND programa = 'INGENIERIA DE SISTEMAS'::text
and proyecto.va_saber_pro.periodo in(
'20163',
'20173',
'20183'
);

--> Total 119

--> Total 269 Sin cruces previamente

-- Ahora bien haciendo el cruce de las pruebas saber 11 y pro
-- de estudiantes de Sistemas/Udenar tomando en cuanta ya los puntajes escalados

select pro.* from proyecto.va_saber_pro pro join proyecto.va_cruces c
ON pro.estu_consecutivo = c.estu_consecutivo_pro
where punt_competencias_ciudadanas_escala is not null;

-- Finalmente se obtiene valor agregado de ciudadanas
-- (promedio(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(promedio(s11.punt_sociales)/max(s11.punt_sociales))
select substring('periodo',0,3)
  --cruces ecaes , sb11 cantidad ,descripcion
  SELECT
    --s11.periodo AS periodo_s11,
    substring(pro.periodo,0,5) AS periodo_ecaes,
    count(1),
    min(pro.punt_competencias_ciudadanas_escala)min_punt_competencias_ciudadanas_escala,
    max(pro.punt_competencias_ciudadanas_escala)max_punt_competencias_ciudadanas_escala,
    avg(pro.punt_competencias_ciudadanas_escala)prom_punt_competencias_ciudadanas_escala,
    min(s11.punt_sociales)min_punt_sociales,
    max(s11.punt_sociales)max_punt_sociales,
    avg(s11.punt_sociales) prom_punt_sociales,
    (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales)/max(s11.punt_sociales)) dif
   FROM proyecto.va_saber_pro pro
     JOIN proyecto.va_cruces c ON pro.estu_consecutivo = c.estu_consecutivo_pro
     JOIN proyecto.va_saber_11 s11 ON s11.estu_consecutivo = c.estu_consecutivo_11
  WHERE pro.departamento_programa = 'NARIÑO'::text 
  AND pro.nombre_institucion ~~* 'UNIVERSIDAD DE NARIÑO-PASTO'::text AND pro.programa = 'INGENIERIA DE SISTEMAS'::text
  group BY  substring(pro.periodo,0,5)
 order by (avg(pro.punt_competencias_ciudadanas_escala)/ max(pro.punt_competencias_ciudadanas_escala)) -(avg(s11.punt_sociales)/max(s11.punt_sociales)) desc;