## Carga de archivos txt del Icfes a PosgreSql
## 1. Se cargo los archivos al servidor a través de FTP
## 2. Se ejecuta el Script de Python para cargar los archivos
## a la base de datos

#!/usr/bin/env python
# coding: utf-8

# In[1]:


from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession


#sc = SparkContext.getOrCreate()
#spark = SparkSession(sc)

spark = SparkSession.builder                     .master("spark://10.10.10.117:7077")                     .appName('write_data')                    .config("spark.jars", "D:\VA\driverdb\\postgresql-42.2.5.jre6.jar")                     .getOrCreate()


# In[2]:


spark


# In[19]:


archivo="FTP_SABER11_20091"


# In[20]:


##
## Row representa una fila en un RDD
##
from pyspark.sql import Row
##
## Crea un DataFrame a partir del archivo con
## formato CSV
##
#df = spark.read.load("D:\\VA\\PQR_20202100153262\\"+archivo+".TXT",

df = spark.read.load("D:\\VA\\PQR_20202100153262\\"+archivo+".txt",
                     format="csv",
                     sep="Â¬",
                     inferSchema= True,
                     encoding='latin1',
                     decimal=",",
                     header="true")
df.printSchema()


# In[21]:


df.count()


# In[22]:


df.write.jdbc(url="jdbc:postgresql://10.10.10.117:5432/proyecto_va"
                  "?user=postgres&password=Grias*20",
              table="archivos."+archivo.lower(),
              mode="append",
              properties={"driver": 'org.postgresql.Driver'})

